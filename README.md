# One Pixel At A Time
by Sejo 

This document is shared to you with the [CC BY-SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/)

## General
* Share your results with the hashtag #OnePixelCamp !

## Plan for the session
* Rationale for this session
	* Introduction to basic building blocks of computation
		* Sequential structures, branching structures (conditional statements), repetitive structures
		* Everything that a computer did, does or can do, is expressed with them
	* Applying them to understand and build image filters - _Cool!_
	* Freedom!
* Pre-coding: Digital image
	* Pixels array
	* Width and height
	* Read and write an individual pixel
	* Color models: RGB and HSB
	* Processing Color Selector
* Basic structure of our Processing sketch
	* Locating the sketch and the image to process: Save the sketch first and then save the image in the same folder
	* We are using Processing in _static_ mode: Program gets executed only once
	* Declaration of variables
	* General setup and initialization of variables
	* Output: Draw and save (and exit)
	* Image processing: let's make green some pixels
	* How to (easily) work with all the pixels?
* Repetition, repetition, repetition
	* What does it mean _to count_?
	* Initial value
	* Upper limit
	* While loop
		* Structure
		* Let's remove two color channels
	* A specific case of the While loop: For loop
	* **Playing with pixels and color models**
* Conditional statements: What if... \[we go non-linear?\]
	* Thresholds!
	* Highlighting or detecting brightness
	* How to highlight hues?
	* Logic operators: AND, OR
	* Loading two images

## Processing sketches 
* [Templates](https://framagit.org/eccs/OnePixelAtATime/tOnePixelAtATime/tree/master/sketches): Follow this link to get the templates that we discussed
* [Session sketches](https://framagit.org/eccs/OnePixelAtATime/tOnePixelAtATime/tree/master/sessionSketches) Here are/will be the sketches that we code during the session

## Useful code blocks
Get Red, Green, Blue values from pixel number `i` of image `img`:
```java
float r,g,b;

r = red( img.pixels[i] );
g = green( img.pixels[i] );
b = blue( img.pixels[i] );
```

Get Hue, Saturation, Brightness values from pixel number `i` of image `img`:
```java
float hue, sat, bri;

hue = hue( img.pixels[i] );
sat = saturation( img.pixels[i] );
bri = brightness( img.pixels[i] );
```

Assign Red, Green and Blue values to pixel number `i` of image `img`.
The three values can be either variables or constants (numbers)
```java
colorMode(RGB); // This mode is the default, use this line only if you changed it before
img.pixels[i] = color(r, g, b);
```

Assign Hue, Saturation, Brightness values to pixel number `i` of image `img`:
The three values can be either variables or constants (numbers)
```java
colorMode(HSB); 
img.pixels[i] = color(hue, sat, bri);
```

## Example images
* [Trajinera](https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Trajinera_en_canal_Nativitas.jpg/640px-Trajinera_en_canal_Nativitas.jpg) 
(640x430) Info: [https://en.wikipedia.org/wiki/File:Trajinera\_en\_canal\_Nativitas.jpg](https://en.wikipedia.org/wiki/File:Trajinera_en_canal_Nativitas.jpg)
* [Mixquic Mágico](https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Mixquic_M%C3%A1gico_17.jpg/640px-Mixquic_M%C3%A1gico_17.jpg) (640x427) Info: [https://commons.wikimedia.org/wiki/File:Mixquic\_M%C3%A1gico\_17.jpg](https://commons.wikimedia.org/wiki/File:Mixquic_M%C3%A1gico_17.jpg)
* [Chroma key example](https://i1.wp.com/www.carmencitta.me/En/wp-content/uploads/2016/08/carmencitta-Green-Screens-2.jpg?resize=640%2C427) (640x427) From: [https://www.carmencitta.me/En/2016/08/25/green-screens-how-do-they-work/](https://www.carmencitta.me/En/2016/08/25/green-screens-how-do-they-work/)

## Links
* [Processing referece](https://processing.org/reference/)
* [Grayscale conversion](https://en.wikipedia.org/wiki/Grayscale)
* [Posterization](https://en.wikipedia.org/wiki/Posterization) - Reduce amount of tones
* [Blend modes](https://en.wikipedia.org/wiki/Blend_modes)

## Exercises ideas
### Playing with pixels and color models
* Remove (make 0) one of the color components of each pixel
* Change the places of the color components of each pixel
* Average the color components of each pixel
* Multiply by a number the color components of each pixel - _What happens if the number is less than 1? What happens if it's greater than 1?_
* Do the previous ideas but with the components being hue, saturation and brightness
* Use the brightness component of a pixel as its color
* Create a "sepia" effect
* Make a dark image look brighter
* Add randomness to one color component

### Operations with conditional statements
* Convert every pixel to white or black according to a condition - This can be used to detect a light source in an image, or a specific hue
* Posterize an image: Reduce the number of color tones (by default 256, from 0 to 255). For example, you can have only two tones per color channel, 0 and 255. 
* Keep only a specific hue in an image, and make the rest of it grayscale

### Loading two images of the same size
* Get the "difference", addition, or multiplication between them (Some examples of _Blend modes_)
* Create a "masking" process: Draw the pixels of image 1 only when the corresponding pixel in image 2 is white
* Create a "chroma key" process: Draw the pixels of image 1 except when they are of a specific color, in that case draw the corresponding pixel of image 2 

### Extras
* For each pixel, assign the color of another random pixel
* For each pixel, assign the color of another random pixel within a neigborhood
* Shift pixels

