// 1)
PImage img;
float r,g,b;
float hue,sat,bri;

// 2)
size(640,430);
img = loadImage("photo.jpg");

// 3) Process
img.loadPixels();
img.pixels[0] = color(0,255,0);
img.pixels[1] = color(0,255,0);
img.pixels[2] = color(0,255,0);
img.pixels[3] = color(0,255,0);
img.updatePixels();

// 4) Output
image(img, 0,0, width, height);