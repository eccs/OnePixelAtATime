// 1)
PImage img;
float r,g,b,number;
float hue,sat,bri;
int i;
// 2)
size(640,430);
img = loadImage("photo.jpg");
// 3) Process
img.loadPixels();
for( i=0; i < img.width * img.height; i=i+1 ){
  r = red( img.pixels[i]);
  g = green( img.pixels[i]);
  b = blue( img.pixels[i]);
  number = (r + g + b)/3;
  //
 // colorMode(HSB);
  img.pixels[i] = color(r*20, g*20, b*20);
}
img.updatePixels();
// 4) Output
image(img, 0,0, width, height);
save("result.jpg");