// Basic template for image processing
// With a loop
// Written by Sejo Vega-Cebrián
// 2017-06-16

// ***********************************
// 1) Declaration of variables
// ***********************************
// Variable for the image
PImage img;
// Index for the pixels of the image
int i;
// Red, green, and blue components of a pixel
float r, g, b;

// ***********************************
// 2) General setup and initialization of variables
// ***********************************
// Create canvas of the same size as the image
size(640,340);

// Load image into variable img
img = loadImage("chichen.jpg");

// ***********************************
// 3) Image processing
// ***********************************
// "Open" the pixels of the image
img.loadPixels();

// Loop through all the pixels of the image
// with index variable "i":

// Start from 0...
i = 0;
// Repeat the following code block
// while the condition is true
// (while index is less than the total number of pixels
while( i < img.width * img.height ){
	// Get the colors of the pixel #i
	r = red(img.pixels[i]);
	g = green(img.pixels[i]);
	b = blue(img.pixels[i]);

	// Process would go here:
	//

	// Update pixel with a new color
	img.pixels[i] = color(r,g,b);

	// Increase the index by 1
	i = i + 1; // This expression is equivalent to: i++;
}

// "Close" and update the pixels of the image
img.updatePixels();


// ***********************************
// 4) Draw and save
// ***********************************
// Draw image in canvas, with its top-left corner in 0,0
image(img,0,0);

// Save the resulting image as "result.jpg"
save("result.jpg");

// Optional: Exit after saving
//exit();
