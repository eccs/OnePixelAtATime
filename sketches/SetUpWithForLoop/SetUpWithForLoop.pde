// Basic template for image processing
// With a for loop
// Written by Sejo Vega-Cebrián
// 2017-06-16

// ***********************************
// 1) Declaration of variables
// ***********************************
// Variable for the image
PImage img;
// Index for the pixels of the image
int i;
// Red, green, and blue components of a pixel
float r, g, b;
// Hue, saturation, brightness of a pixel
float hue, sat, bri;

// ***********************************
// 2) General setup and initialization of variables
// ***********************************
// Create canvas of the same size as the image
size(640,340);

// Load image into variable img
img = loadImage("chichen.jpg");

// ***********************************
// 3) Image processing
// ***********************************
// "Open" the pixels of the image
img.loadPixels();

// Loop through all the pixels of the image
// with index variable "i":

// Start from 0,
// and while i is less than the total number of pixels
// increase i by 1
for( i=0; i < img.width * img.height; i++){
	// Get the colors of the pixel #i
	// in RGB and in HSB
	r = red(img.pixels[i]);
	g = green(img.pixels[i]);
	b = blue(img.pixels[i]);

	hue = hue(img.pixels[i]);
	sat = saturation(img.pixels[i]);
	bri = brightness(img.pixels[i]);

	// Process would go here:
	//
	colorMode(HSB);
	r = 20;
	g = sat + 20;
	b = bri;

	// Update pixel with a new color
	img.pixels[i] = color(r,g,b);
}

// "Close" and update the pixels of the image
img.updatePixels();


// ***********************************
// 4) Draw and save
// ***********************************
// Draw image in canvas, with its top-left corner in 0,0
image(img,0,0);

// Save the resulting image as "result.jpg"
save("result.jpg");

// Optional: Exit after saving
//exit();
