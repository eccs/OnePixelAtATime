// Basic template for image processing
// Written by Sejo Vega-Cebrián
// 2017-06-16

// ***********************************
// 1) Declaration of variables
// ***********************************
// Variable for the image
PImage img;

// ***********************************
// 2) General setup and initialization of variables
// ***********************************
// Create canvas of the same size as the image
size(640,340);

// Load image into variable img
img = loadImage("chichen.jpg");

// ***********************************
// 3) Image processing
// ***********************************
// "Open" the pixels of the image
img.loadPixels();

// Process would go here:


// "Close" and update the pixels of the image
img.updatePixels();


// ***********************************
// 4) Draw and save
// ***********************************
// Draw image in canvas, with its top-left corner in 0,0
image(img,0,0);

// Save the resulting image as "result.jpg"
save("result.jpg");

// Optional: Exit after saving
//exit();
